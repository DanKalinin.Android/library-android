//
// Created by Dan Kalinin on 9/11/20.
//

#include "LAJNI.h"

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, void *reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_2);
}
