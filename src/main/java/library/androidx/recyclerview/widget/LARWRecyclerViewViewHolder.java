package library.androidx.recyclerview.widget;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class LARWRecyclerViewViewHolder extends RecyclerView.ViewHolder {
    public LARWRecyclerViewViewHolder(View itemView) {
        super(itemView);
    }
}
