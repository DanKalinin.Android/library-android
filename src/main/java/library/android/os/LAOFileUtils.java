package library.android.os;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LAOFileUtils {
    public static int copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        int ret = 0;

        byte[] b = new byte[1024];
        int n = 0;
        while ((n = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, n);
            ret += n;
        }

        return ret;
    }
}
