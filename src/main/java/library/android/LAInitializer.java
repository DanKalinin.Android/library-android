package library.android;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.java.LJInitializer;
import library.java.lang.LJLObject;

public class LAInitializer extends LJLObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-android");
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(LJInitializer.class);
        return ret;
    }
}
